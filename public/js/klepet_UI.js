/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://teaching.lavbic.net/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  };
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://teaching.lavbic.net/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}
function dodajVideo(vhodnoBesedilo){
  var id;
  var frame = ' ';
  if (vhodnoBesedilo.includes("watch?v=")) {
  	console.log("yes");
    var video_id = vhodnoBesedilo.split(' ');
    console.log(video_id);
    for (var i=0; i<video_id.length; i++) {
    	id = video_id[i].split('v=')[1];
        if (id != undefined) {
        	console.log("id=" + id);
          frame += '<iframe style="margin-left: 10px; margin-top: 10px;" id="player" type="text/html" width="200px" height="150px" src="https://www.youtube.com/embed/' + id + '" allowfullscreen></iframe>';
          vhodnoBesedilo = vhodnoBesedilo + '<p>';
        }
    }
    console.log("fr=" + frame);
    vhodnoBesedilo = vhodnoBesedilo + frame;
  }
  return vhodnoBesedilo;
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
var uporabnikii;
var kosi;
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://teaching.lavbic.net/OIS/gradivo/") > -1;
  var jeOmemba = sporocilo.indexOf("&#9758;") > -1;
  var jeVideo = sporocilo.indexOf("https://www.youtube.com/") > -1;
  if (jeSmesko || jeOmemba || jeVideo) { // || jeVideo
    sporocilo =
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;p").join("<p")
               .split("&gt;").join(">")
               .split("&lt;img").join("<img")
               .split("&lt;iframe").join("<iframe")
               .split("allowfullscreen &gt;").join("allowfullscreen >")
               .split("&lt;/iframe &gt;").join("</iframe >")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  console.log(sporocilo);
  sporocilo = dodajSmeske(sporocilo);
  //sporocilo = dodajVideo(sporocilo);
  var sistemskoSporocilo;
  
  kosi = sporocilo.split(" ");
  console.log(kosi);
  console.log(uporabnikii);
  
  
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtrirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
  var omemba = '&#9758; Omemba v klepetu';
    for (var i=0; i<kosi.length; i++) {
      for (var j=0; j<uporabnikii.length; j++) {
        if (kosi[i] == '@' + uporabnikii[j] && uporabnikii[j] != trenutniVzdevek) {
        console.log("yes");
        this.socket.emit('sporocilo', {vzdevek: uporabnikii[j], besedilo: omemba});
        }
      }
    }
}


// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('./swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n');
});


/**
 * Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj 
 * z enako dolžino zvezdic (*)
 * 
 * @param vhodni niz
 */
function filtrirajVulgarneBesede(vhod) {
  for (var i in vulgarneBesede) {
    var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
    vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
  }
  return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";


// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
    //var video = dodajVideo(sporocilo.besedilo);
    //$('#sporocila').append(video);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(uporabniki) {
    $("#seznam-uporabnikov").empty();
    for (var i=0; i < uporabniki.length; i++) {
      $("#seznam-uporabnikov").append(divElementEnostavniTekst(uporabniki[i]));
      uporabnikii = uporabniki;
    }
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    console.log("");
    return false;
  });
});


/* global $, io, Klepet */